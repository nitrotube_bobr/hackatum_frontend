const config = {};

// eslint-disable-next-line no-undef
config.backendURL = process.env.REACT_APP_BACKEND_URL;
config.accessToken = process.env.REACT_APP_MAPBOX_TOKEN;
config.iFrameURL = process.env.REACT_APP_IFRAME_URL;

export default config;
