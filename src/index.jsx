import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import "./styles/index.css";
import "mapbox-gl/dist/mapbox-gl.css";

ReactDOM.render(
	<React.StrictMode>
		<App style={{ height: "100vh" }} />
	</React.StrictMode>,
	document.querySelector("#root"),
);
