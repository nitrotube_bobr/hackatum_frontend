import React, { useRef, useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
// eslint-disable-next-line import/no-unresolved
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import config from "../utils/config";

mapboxgl.accessToken = "pk.eyJ1IjoibnQtZmEiLCJhIjoiY2t0dHBvdmtvMGFwbjJwcXRndmY3cjdiaiJ9.f7EI4dg15enSn1Q20e5ipA";

const Root = styled("div")({});
const MapContainer = styled("div")({
	height: "400px",
});

function MainMap() {
	const mapContainer = useRef(null);
	const map = useRef(null);
	const [lng, setLng] = useState(-70.9);
	const [lat, setLat] = useState(42.35);
	const [zoom, setZoom] = useState(9);
	useEffect(() => {
		if (map.current) return; // initialize map only once
		map.current = new mapboxgl.Map({
			container: mapContainer.current,
			style: "mapbox://styles/mapbox/streets-v11",
			center: [lng, lat],
			zoom,
		});
		map.current.on("move", () => {
			setLng(map.current.getCenter().lng.toFixed(4));
			setLat(map.current.getCenter().lat.toFixed(4));
			setZoom(map.current.getZoom().toFixed(2));
		});
	});
	return (
		<Root>
			<MapContainer ref={mapContainer} />
		</Root>
	);
}

export default MainMap;
